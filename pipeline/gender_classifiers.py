'''
    Created at Nov 2020
    Author: Yuan-Chi Yang
    Objective:
        - this python file contains all the classifiers for gender detection for deployment
          
'''
import pandas as pd
import numpy as np
import json
import os
import pickle

dir_path = os.path.dirname(__file__)

label_classes = ['F','M']

# For SVM on tweets
import re
    
def preprocess_text(tweet_text):
    '''
        This function performs preprocessing on the tweets

        Params: tweet_text: the tweet before preprocessing
        Return: tweet_text: the tweet after preprocessing
    '''
    tweet_text = re.sub(r'&amp;', "and", tweet_text)  
    tweet_text = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '_url_',
                        tweet_text)
    tweet_text = re.sub('(@[A-Za-z0-9\_]+)', '_username_', tweet_text)
    tweet_text = re.sub(r'\x00', " ", tweet_text)
    tweet_text = re.sub('n(\'|’)t', ' not', tweet_text)
    tweet_text = tweet_text.lower()
    tweet_text = re.sub(r'\s{2,}', " ", tweet_text)

    return tweet_text



def tweets_svm_funx(user_dicts, if_score=False):
    '''
        This function uses the trained vectorizer and svm classifier to classify the user's gender using tweets

        Params:
            user_dicts: the list of dictionaries, each representing the user's profile.
                        the user's tweets need to be concatenated and stored in field 'tweets'
                        the field 'tweets' can't be empty or None
            if_score: if return scores
        Return:
            list of predicted gender
            (optional, if_score=True) list of prediction scores
    '''
    import sys
    sys.path.append(fr"{dir_path}/models/tweets_svm/")
    #from happierfuntokenizing import Tokenizer

    foldername = f"{dir_path}/models/tweets_svm"

    f_vectorizer = open(f'{foldername}/vectorizer', 'rb')
    tweets_svm_vectorizer = pickle.load(f_vectorizer)

    f_model = open(f'{foldername}/model', 'rb')
    tweets_svm_model = pickle.load(f_model)
    
    tweets = [user_dict['tweets'] for user_dict in user_dicts]

    processed_tweets = [preprocess_text(tweet) for tweet in tweets]
    data_vectors = tweets_svm_vectorizer.transform(processed_tweets).toarray()
    data_combined_vectors = data_vectors
    X = data_combined_vectors
    Y_pred = tweets_svm_model.predict(X)
    if if_score:
        Y_score = tweets_svm_model.decision_function(X)
        return list(Y_pred), Y_score
    else:
        return list(Y_pred)



# M3
from m3inference import M3Inference
from m3inference.utils import get_lang
# using only the text mode
m3 = M3Inference(use_full_model=False)
def transformed_funx(user_dicts, file_path='./user_dicts_transformed.jsonl'):
    '''
            This function transform the user's data (a json file) into the input format of m3 system
            Params:
                user_dicts: the list of dictionaries, each representing the user's profile.
                            the user's tweets need to be concatenated and stored in field 'tweets'
                            the field 'tweets' can't be empty or None
                file_path: the temporary storage of the user's transformed data for m3 system
            Return:
                None
        '''
    user_dicts_transformed = []
    for user in user_dicts:
        bio = user.get("description",None)
        if bio == None:
            bio = ""

        if bio == "":
            lang = "un"
        else:
            lang = get_lang(bio)

        # (2/3/2021) let all the empty name be an empty string
        name = user.get("name",None)
        if name == None:
            name = ""

        output = {
            "description": bio,
            "id": user["id_str"],
            "lang": lang,
            "name": name,
            "screen_name": user.get("screen_name",None)
        }
        user_dicts_transformed.append(output)

    with open(file_path,'w') as f:
        for data in user_dicts_transformed:
            f.write(json.dumps(data)+'\n')



def m3_profile_funx(user_dicts, if_score=False):
    '''
            This function uses the m3 system to classify the user's gender using the user's profile, including descirption, name, and screen name

            Params:
                user_dicts: the list of dictionaries, each representing the user's profile.
                            the user's tweets need to be concatenated and stored in field 'tweets'
                            the field 'tweets' can't be empty or None
                if_score: if return scores
            Return:
                list of predicted gender
                (optional, if_score=True) list of prediction scores
    '''
    file_path = './user_dicts_transformed.jsonl'
    transformed_funx(user_dicts, file_path=file_path)
    pred = m3.infer(file_path)

    Y_pred = []
    Y_score = []
    for user_id in pred.keys():
        user_dict = pred[user_id]
        gender_dict = user_dict['gender']
        M_score = gender_dict['male']
        F_score = gender_dict['female']

        if M_score >= F_score:
            gender = 'M'
        else:
            gender = 'F'
        Y_pred.append(gender)
        Y_score.append(M_score)

    Y_score = np.array(Y_score)
    if if_score:
        return Y_pred, Y_score
    else:
        return Y_pred

#Meta1
def meta1_funx(user_dicts=None, tweets_svm_score = None, m3_profile_score = None, if_score=False):
    '''
        This function uses the meta1 to classify the user's gender

        Params:
            tweets_svm_score: the scores given by tweets_svm_funx. If the scores are not given, the system will call tweets_svm_funx
            m3_profile_score: the scores given by m3_profile_funx. If the scores are not given, the system will call tweets_svm_funx
            user_dicts: the user's data. It only needs to be provided if one of the scores are not given
            if_score: if return scores
        Return:
            list of predicted gender
            (optional, if_score=True) list of prediction scores
    '''
    foldername = f"{dir_path}/models/meta1"
    f_meta1 = open(f'{foldername}/model_meta1', 'rb')
    mode_meta1= pickle.load(f_meta1)

    if tweets_svm_score is None:
        tweets_svm, tweets_svm_score = tweets_svm_funx(user_dicts, if_score=True)

    if m3_profile_score is None:
        m3_profile, m3_profile_score = m3_profile_funx(user_dicts, if_score=True)


    X = np.transpose(np.array([tweets_svm_score, m3_profile_score]))
    assert X.shape == (len(tweets_svm_score), 2)
    Y_pred = mode_meta1.predict(X)

    if if_score:
        Y_score = mode_meta1.decision_function(X)
        return list(Y_pred), Y_score
    else:
        return list(Y_pred)


if __name__ == "__main__":
    # import the testing data set
    test_file = 'testing_set.json'
    user_dicts = []
    with open(test_file, 'r') as f:
        for line in f.readlines():
            user_dicts.append(json.loads(line))

    tweets_svm, tweets_svm_score = tweets_svm_funx(user_dicts, if_score=True)
    print("Tweets_SVM:")
    print("predictions = ",tweets_svm)
    print("scores      = ", tweets_svm_score)
    print("-" * 40)

    m3_profile, m3_profile_score = m3_profile_funx(user_dicts, if_score=True)
    print("M3_profile:")
    print("predictions = ",m3_profile)
    print("scores      = ", m3_profile_score)
    print("-"*40)
        
    meta1, meta1_score = meta1_funx(user_dicts = user_dicts, if_score = True)
    print("Meta1:")
    print("predictions = ", meta1)
    print("scores      = ", meta1_score)
    print("-" * 40)

    meta1, meta1_score = meta1_funx(tweets_svm_score = tweets_svm_score, m3_profile_score=m3_profile_score, if_score = True)
    print("Meta1:")
    print("predictions = ", meta1)
    print("scores      = ", meta1_score)
    print("-" * 40)

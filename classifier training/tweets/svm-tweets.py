'''
    Created on Jan, 2021
    Author: Yuan-Chi Yang
    Objective: This script is used to experiment with SVM algorithm on gender detection using tweets.
'''

import pandas as pd
import numpy as np

import re
import sklearn

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import LinearSVC
from sklearn.metrics import precision_recall_fscore_support, accuracy_score

from happierfuntokenizing import Tokenizer

def show_result(Y_true, Y_pred,label_classes, output_file=None):
    '''
        This function print the coverage, precision, recall, fscore, support, and accuracy according to label_classes
        
        input: 
            Y_true: the true label
            Y_pred: the predicted label
            label_classes: [0,1]
            
        output:
            print the performance
    '''        
    precision, recall, fscore, support = precision_recall_fscore_support(Y_true, Y_pred, labels = label_classes)
    accuracy = accuracy_score(Y_true, Y_pred)
    
    if output_file:
        print('class = \t {:4} {:4}'.format(*label_classes),file = output_file)
        print('precision = \t {:4.2f} {:4.2f}'.format(*precision),file=output_file)
        print('recall = \t {:4.2f} {:4.2f}'.format(*recall),file=output_file)
        print('fscore = \t {:4.2f} {:4.2f}'.format(*fscore),file=output_file)
        print('support =\t {:4d} {:4d}'.format(*support),file=output_file)
        
        print('accuracy = \t {:4.1%}'.format(accuracy),file=output_file)
        print('-----------------------------------------------------\n')
    else:
        print('class = \t {:4} {:4}'.format(*label_classes))
        print('precision = \t {:4.2f} {:4.2f}'.format(*precision))
        print('recall = \t {:4.2f} {:4.2f}'.format(*recall))
        print('fscore = \t {:4.2f} {:4.2f}'.format(*fscore))
        print('support =\t {:4d} {:4d}'.format(*support))
        
        print('accuracy = \t {:4.1%}'.format(accuracy))
        print('-----------------------------------------------------\n')

if __name__ == "__main__":
    start_time = time.localtime()
    max_iter = 50000
    max_features_list = [20000] 
    n_gram_max_list = [1]
    C_list = [32]
    min_num_tweets_list = [100]

    token_pattern = None
    tokenizer = Tokenizer()
    
    train_file = # path_to_train_data
    val_file = # path_to_val_data

    train_dist = # path_to_train_distribution
    
   
    # Reading training file
    df_train_all = pd.read_csv(train_file,header=0,keep_default_na=False,
                           usecols=['user_id', 'text', 'label'], dtype = {'user_id':str})
    df_dist = pd.read_csv(train_dist, header = 0, dtype={'user_id':str})
    assert df_dist.user_id.to_list() == df_train_all.user_id.to_list()
    df_train_all = df_train_all.merge(df_dist, on = 'user_id', how = 'left')
    
    # Reading validation file
    df_val = pd.read_csv(val_file,header=0,keep_default_na=False,
                         usecols = ['user_id', 'text', 'label'], dtype = {'user_id':str})
    val_classes = df_val['label']
    val_texts = df_val['text']
    
    label_classes = sorted(df_train_all['label'].unique())
    print(label_classes)
    
    
    for min_num_tweets in min_num_tweets_list:
        # generate df_train by limiting #_tweets
        # return a new dataframe
        df_train = df_train_all[df_train_all['#_tweets']>= min_num_tweets].reset_index(drop=True).copy()
        train_classes = df_train['label']
        train_texts = df_train['text']

        for max_features in max_features_list:
            for n_gram_max in n_gram_max_list:
                print(f'max_features = {max_features}')    
                print(f'n_gram_max = {n_gram_max}')    
                print('Now calculate Bag of Words at',time.asctime())
                # use TfidfVectorizer for frequency
                vectorizer = TfidfVectorizer(ngram_range=(1,n_gram_max), analyzer="word", tokenizer=tokenizer.tokenize, preprocessor=None,
                                             max_features=max_features, binary=False, token_pattern=token_pattern,
                                             use_idf=False, norm='l1')
                
                train_data_vectors = vectorizer.fit_transform(train_texts).toarray()
                train_data_combined_vectors = train_data_vectors  # np.concatenate((training_data_vectors, training_data_cluster_vectors), axis=1)

                val_data_vectors = vectorizer.transform(val_texts).toarray()
                val_data_combined_vectors = val_data_vectors

                t_end = time.time()
                print('Takes {} s'.format(t_end - t_str))
                
                X_train = train_data_combined_vectors
                X_val = val_data_combined_vectors
                Y_train = train_classes
                Y_val = val_classes
                 
                for C in C_list:
                    model = LinearSVC(C=C, tol=1e-5, max_iter = max_iter, random_state = 123,verbose = 1,
                                      penalty='l1',loss='squared_hinge', dual = False)
                    model.fit(X=X_train, y=Y_train)
                    
                    Y_train_pred = model.predict(X_train)
                    Y_val_pred = model.predict(X_val)
                    
                    print('Training Performance:')
                    show_result(Y_train, Y_train_pred, label_classes)

                    print('Validation Performance:')
                    show_result(Y_val, Y_val_pred, label_classes)

      
    print('Finished!!')



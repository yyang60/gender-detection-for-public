'''
    Created on Aug, 2020
    Author: Yuan-Chi Yang
    Objective: This script is used to experiment with BERT algorithm on gender detection using description.
    Note:
        Labels are represented by numbers as label_map = {'F':0,'M':1}
        This script is based on package, 'simpletransformers,' which is based on the Transformers library by HuggingFace
'''


from simpletransformers.classification import ClassificationModel
import pandas as pd
import sklearn

from sklearn.metrics import precision_recall_fscore_support, accuracy_score

def predict_label_prob(model, df_in):
    '''
        This function makes prediction based on trained BiLSTM model
        
        Input:
            model: the BERT model
            
            df_in: the dataframe for the data to be predicted. The preprocessed text should be stored as the column 'text'
            
        output:
            df_data: a dataframe with column 'labels_pre' for the predicted column
                                             'prob_i' for the probability to be labeled as i
    '''
    data_dict = dict()
    
    y_hat, y_hat_prob = model.predict(df_in['text'])
     
    data_dict['labels_pre'] = y_hat
    
    for i in range(0,y_hat_prob.shape[1]):
        data_dict['prob_'+str(i)] = y_hat_prob[:,i]
    
    df_data = pd.DataFrame(data_dict)
    
    return df_data
	
def show_result(Y_true, Y_pred,label_classes, output_file=None):
    '''
        This function print the coverage, precision, recall, fscore, support, and accuracy according to label_classes
        
        input: 
            Y_true: the true label
            Y_pred: the predicted label
            label_classes: [0,1]
            
        output:
            print the performance
    '''
    precision, recall, fscore, support = precision_recall_fscore_support(Y_true, Y_pred, labels = label_classes)
    accuracy = accuracy_score(Y_true, Y_pred)
    
    if output_file:
        print('class = \t {:4d} {:4d}'.format(*label_classes),file = output_file)
        print('precision = \t {:4.2f} {:4.2f}'.format(*precision),file=output_file)
        print('recall = \t {:4.2f} {:4.2f}'.format(*recall),file=output_file)
        print('fscore = \t {:4.2f} {:4.2f}'.format(*fscore),file=output_file)
        print('support =\t {:4d} {:4d}'.format(*support),file=output_file)
        
        print('accuracy = \t {:4.1%}'.format(accuracy),file=output_file)
        print('-----------------------------------------------------\n')
    else:
        print('class = \t {:4d} {:4d}'.format(*label_classes))
        print('precision = \t {:4.2f} {:4.2f}'.format(*precision))
        print('recall = \t {:4.2f} {:4.2f}'.format(*recall))
        print('fscore = \t {:4.2f} {:4.2f}'.format(*fscore))
        print('support =\t {:4d} {:4d}'.format(*support))
        
        print('accuracy = \t {:4.1%}'.format(accuracy))
        print('-----------------------------------------------------\n')

if __name__ == "__main__":

    input_file = # the file path to the training data
    val_file = # the file path to the validation data
    test_file = # the file path to the test data
   
    df_train= pd.read_csv(input_file,header = 0,keep_default_na=False)
    df_val= pd.read_csv(val_file,header = 0,keep_default_na=False)
    df_test= pd.read_csv(test_file,header = 0,keep_default_na=False)

    label_classes = sorted(df_train['labels'].unique())
    print(label_classes)

    model_type = 'roberta' 
    model_name = 'roberta-large' 
    num_train_epochs = 3
    input_args ={'max_seq_length':128, 'train_batch_size': 32, 'reprocess_input_data': True, 'overwrite_output_dir': True,'num_train_epochs': num_train_epochs,'learning_rate':5e-6} 
    if_use_cuda = True
    model = ClassificationModel(model_type = model_type, 
                                model_name = model_name, 
                                num_labels=len(label_classes), 
                                args=input_args,
                                use_cuda=if_use_cuda) 
    

    _ = model.train_model(train_df = df_train)

    df_output = pd.concat([df_train[['user_id','labels']], predict_label_prob(model, df_train)],axis=1)
    df_val_out = pd.concat([df_val[['user_id','labels']], predict_label_prob(model, df_val)],axis=1)
    df_test_out = pd.concat([df_test[['user_id','labels']], predict_label_prob(model, df_test)],axis=1)

    print('Training Performance:')
    show_result(df_output['labels'], df_output['labels_pre'],label_classes)
    
    print('Validation Performance:')
    show_result(df_val_out['labels'], df_val_out['labels_pre'],label_classes)

    print('Test Performance:')
    show_result(df_test_out['labels'], df_test_out['labels_pre'],label_classes)

    
      



